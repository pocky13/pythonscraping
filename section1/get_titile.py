from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
from bs4 import BeautifulSoup


def get_title(url):
	try:
		html = urlopen(url)
	except HTTPError as e:#サーバからエラーコードが返ってきた場合  http://www.pythonscraping.com/pages/pagaaaae1.html
		print(e)
		return None
	except URLError as e:#ドメイン自体が間違っており、サーバに到達できなかった場合   http://www.pythonscaaaraping.com/pages/page1.html
		print(e)
		return None

	try:
		bsObj = BeautifulSoup(html.read(), 'lxml')
		title = bsObj.body.h1
	except AttributeError as e:#h1タグが存在しない場合
		return None

	return title


def main():
	title = get_title("http://www.pythonscraping.com/pages/page1.html")
	if title is None:
		print("title could not be found")
	else:
		print(title)


if __name__ == "__main__":
	main()
