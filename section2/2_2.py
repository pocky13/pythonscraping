from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen("http://www.pythonscraping.com/pages/warandpeace.html")

bsObj = BeautifulSoup(html.read(), "lxml")

nameList = bsObj.find_all("span", {"class": "green"})  # green クラスのspanタグで囲まれたもののリスト
for name in nameList:
	print(name.get_text())#タグを除外してテキストを返す

print(type(name))
